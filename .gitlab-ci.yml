image: node:latest

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"

stages:
  - build
  - deploy
  - manual
  - performance
  - copy-sample-files
  # Has to be named this way because the included security-reports 
  # .gitlab-ci.yml file expects a stage named 'test'. This stage
  # produces security findings and is used for the Security Reports widget.
  - test
  - accessibility
  - metrics
  

cache:
  paths:
    - node_modules/
    - .cache/
    - public/

include:
  - template: Verify/Accessibility.gitlab-ci.yml
  - template: Jobs/Code-Quality.gitlab-ci.yml
  - remote: https://gitlab.com/gitlab-examples/security/security-reports/-/raw/main/.gitlab-ci.yml

# Test summary
test_summary:
  stage: test
  before_script:
    - export FILENAME=$([ "$CI_COMMIT_REF_SLUG" == main ] && echo "main" || echo "feature-branch")
  script:
    - cp -v mock_test_reports/"$FILENAME".xml ./junit.xml
  artifacts:
    reports:
      junit: junit.xml

# Metrics
metrics:
  stage: metrics
  script:
    - echo 'static_text_metric some_string_value' >> metrics.txt
    - echo 'static_integer_metric 654' >> metrics.txt
    - echo 'static_float_metric 123.456' >> metrics.txt
    - echo 'random_text_metric' $(( $RANDOM )) | tr '[0-9]' '[a-z]' >> metrics.txt
    - echo 'random_integer_metric' $(( $RANDOM % 100 )) >> metrics.txt
    - echo 'random_float_metric' $(( $RANDOM % 100 )).$(( $RANDOM % 100 )) >> metrics.txt
    - echo 'infinite_metric +Inf' >> metrics.txt
    - "[[ \"$CI_COMMIT_REF_NAME\" != \"$CI_DEFAULT_BRANCH\" ]] && echo 'always_new_metric new_value' >> metrics.txt"
  artifacts:
    reports:
      metrics: metrics.txt

# Copy sample files from the remote repository. This files contains
# mock vulnerability findings, which are used to produce mock data.
copy_security_reports_sample_files:
  stage: copy-sample-files
  script:
    - echo $CI_PROJECT_DIR
    - git clone https://gitlab.com/gitlab-examples/security/security-reports.git
    - cd security-reports
    - cp -R ./samples $CI_PROJECT_DIR
  artifacts:
    paths:
      - samples

.terraform-plan-generation:
  stage: build
  before_script:
    - mkdir -p ${TERRAFORM_DIRECTORY}
    - cd ${TERRAFORM_DIRECTORY}
  script:
    - if [ "$CI_JOB_NAME" = "failed_report" ]; then echo "{}" > terraform_states.json; else echo "{\"create\":\"$((RANDOM % 10))\",\"update\":\"$((RANDOM % 10))\",\"delete\":\"$((RANDOM % 10))\"}" > terraform_states.json; fi
  artifacts:
    reports:
      terraform: ${TERRAFORM_DIRECTORY}/terraform_states.json

succedded_report:
  extends: .terraform-plan-generation
  variables:
    TERRAFORM_DIRECTORY: "succedded"

failed_report:
  extends: .terraform-plan-generation
  variables:
    TERRAFORM_DIRECTORY: "failed"

code_quality:
  rules:
    - if: $CODE_QUALITY_DISABLED
      when: never
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" # Run code quality job in merge request pipelines
    - if: $CI_COMMIT_TAG                               # Run code quality job in pipelines for tags